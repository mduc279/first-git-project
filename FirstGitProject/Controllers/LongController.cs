﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstGitProject.Controllers
{
    public class LongController : Controller
    {
        // GET: Long
        public ActionResult Index()
        {
            return View();
        }

        // GET: Long/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Long/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Long/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Long/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Long/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Long/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Long/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
